package com.atlassian.bamboo.hipchat;

import com.atlassian.bamboo.builder.BuildState;
import com.atlassian.bamboo.notification.Notification;
import com.atlassian.bamboo.notification.NotificationTransport;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.utils.HttpUtils;
import com.atlassian.event.Event;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class HipchatNotificationTransport implements NotificationTransport
{
    private static final Logger log = Logger.getLogger(HipchatNotificationTransport.class);
    public static final String FAILED_ICON = "plan_failed_16.png";
    public static final String SUCCESSFUL_ICON = "plan_successful_16.png";
    public static final String UNKNOWN_ICON = "plan_canceled_16.png";
    public static final String HIPCHAT_API_URL = "https://api.hipchat.com/v1/rooms/message?auth_token=";

    private final String apiToken;
    private final String room;
    private final String from = "Bamboo";
    private final boolean notify;

    private final HttpClient client;

    private final Plan plan;
    private final ResultsSummary resultsSummary;
    private final Event event;

    public HipchatNotificationTransport(String apiToken,
                                        String room,
                                        boolean notify,
                                        Plan plan,
                                        ResultsSummary resultsSummary,
                                        Event event)
    {
        this.apiToken = apiToken;
        this.room = room;
        this.notify = notify;
        this.plan = plan;
        this.resultsSummary = resultsSummary;
        this.event = event;
        client = new HttpClient();

        try
        {
            URI uri = new URI(HIPCHAT_API_URL);
            setProxy(client, uri.getScheme());
        }
        catch (URIException e)
        {
            log.error("Unable to set up proxy settings, invalid URI encountered: " + e);
        }
        catch (URISyntaxException e)
        {
            log.error("Unable to set up proxy settings, invalid URI encountered: " + e);
        }
    }

    @Override
    public void sendNotification(Notification notification)
    {

        String message = (notification instanceof Notification.HtmlImContentProvidingNotification)
                ? ((Notification.HtmlImContentProvidingNotification) notification).getHtmlImContent()
                : notification.getIMContent();

        if (!StringUtils.isEmpty(message))
        {
            PostMethod method = setupPostMethod();
            method.setParameter("message",message);
            if (resultsSummary != null)
            {
                setMessageColor(method, resultsSummary);
            }
            else
            {
                setMessageColor(method, "yellow"); //todo: might need to use different color in some cases
            }

            try
            {
                client.executeMethod(method);
            }
            catch (IOException e)
            {
                log.error("Error using Hipchat API: " + e.getMessage(), e);
            }
        }
    }

    private void setMessageColor(PostMethod method, ResultsSummary result)
    {
        String color = "yellow";

        if (result.getBuildState() == BuildState.FAILED)
        {
            color = "red";
        }
        else if (result.getBuildState() == BuildState.SUCCESS)
        {
            color = "green";
        }

        setMessageColor(method, color);
    }

    private void setMessageColor(PostMethod method, String colour)
    {
        method.addParameter("color", colour);
    }

    private PostMethod setupPostMethod()
    {
        PostMethod m = new PostMethod(HIPCHAT_API_URL + apiToken);
        m.addParameter("room_id", room);
        m.addParameter("from", from);
        m.addParameter("notify", (notify ? "1" : "0"));
        return m;
    }

    private static void setProxy(@NotNull final HttpClient client, @Nullable final String scheme) throws URIException
    {
        HttpUtils.EndpointSpec proxyForScheme = HttpUtils.getProxyForScheme(scheme);
        if (proxyForScheme!=null)
        {
            client.getHostConfiguration().setProxy(proxyForScheme.host, proxyForScheme.port);
        }
    }
}
