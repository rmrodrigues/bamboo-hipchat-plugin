package it;

import com.atlassian.bamboo.build.LogEntry;
import com.atlassian.bamboo.event.BuildHungEvent;
import com.atlassian.bamboo.hipchat.HipchatNotificationTransport;
import com.atlassian.bamboo.notification.buildhung.BuildHungNotification;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.project.Project;
import com.atlassian.event.Event;
import com.google.common.collect.Lists;
import junit.framework.TestCase;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;
import org.easymock.EasyMock;

import java.io.IOException;
import java.lang.reflect.Field;

public class HipchatNotificationTransportTest extends TestCase
{
    private static final Logger log = Logger.getLogger(HipchatNotificationTransportTest.class);

    // ------------------------------------------------------------------------------------------------------- Constants
    private final String API_TOKEN = "alamakotaakotmaapitoken";
    private final String ROOM = "theRoom";
    private final boolean NOTIFY_USERS = true;

    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    public void testCorrectUrlsAreHit()
    {
        Project project = EasyMock.createMock(Project.class);
        EasyMock.expect(project.getKey())
                .andReturn("BAM");
        Plan plan = EasyMock.createMock(Plan.class);
        EasyMock.expect(plan.getProject())
                .andReturn(project);
        EasyMock.expect(plan.getBuildKey())
                .andReturn("MAIN");
        EasyMock.expect(plan.getName())
                .andReturn("Main");

        PlanResultKey planResultKey = PlanKeys.getPlanResultKey("BAM-MAIN", 3);

        final BuildHungEvent event = new BuildHungEvent(this, planResultKey.getPlanKey().getKey(), planResultKey.getBuildNumber(), null, Lists.<LogEntry>newArrayList());

        BuildHungNotification notification = new BuildHungNotification()
        {
            public String getHtmlImContent()
            {
                return "IM Content";
            }

            public BuildHungEvent getEvent()
            {
                return event;
            }
        };

        EasyMock.replay(plan, project);

        HipchatNotificationTransport hnt = new HipchatNotificationTransport(API_TOKEN, ROOM, NOTIFY_USERS, plan, null, event);

        //dirty reflections trick to inject mock HttpClient
        try
        {
            Field field = HipchatNotificationTransport.class.getDeclaredField("client");
            field.setAccessible(true);
            field.set(hnt, new MockHttpClient());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            fail(e.getMessage());
        }

        hnt.sendNotification(notification);
    }
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators

    public class MockHttpClient extends org.apache.commons.httpclient.HttpClient
    {
        public int executeMethod(HttpMethod method)
                throws IOException, HttpException
        {

            assertTrue(method instanceof PostMethod);
            PostMethod postMethod = (PostMethod)method;
            assertEquals(HipchatNotificationTransport.HIPCHAT_API_URL + API_TOKEN, method.getURI().toString());
            assertEquals(ROOM, postMethod.getParameter("room_id").getValue());
            assertEquals("Bamboo", postMethod.getParameter("from").getValue());
            assertEquals("1", postMethod.getParameter("notify").getValue());
            return 0;
        }
    }
}
